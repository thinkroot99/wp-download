import os
import requests
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor
import time

# URL-ul site-ului WordPress
wordpress_url = "https://exemplu.ro"

# Directorul în care se vor salva fișierele
output_directory = "/calea/unde/se/salvează/fișierele"

# Verifică dacă directorul există, altfel creează-l
os.makedirs(output_directory, exist_ok=True)

# Creează o sesiune HTTP persistentă
session = requests.Session()

# Funcție pentru a extrage toate linkurile către articolele de pe o pagină
def extract_article_links(page_url):
    try:
        response = session.get(page_url)
        response.raise_for_status()  # Verifică dacă există erori HTTP
        soup = BeautifulSoup(response.content, "html.parser")
        article_links = []
        for link in soup.find_all("a"):
            href = link.get("href")
            if href and href.startswith(wordpress_url) and not "category" in href:
                article_links.append(href)
        return article_links
    except requests.exceptions.HTTPError as e:
        print(f"Eroare HTTP la accesarea paginii {page_url}: {e}")
        return []
    except requests.exceptions.RequestException as e:
        print(f"Eroare la accesarea paginii {page_url}: {e}")
        return []

# Obține toate linkurile către articolele de pe site-ul WordPress
print("Extragerea linkurilor către articole...")
all_article_links = []
page_number = 1
while True:
    page_url = f"{wordpress_url}/page/{page_number}/"
    print(f"Parcurgere pagina {page_number}...")
    article_links = extract_article_links(page_url)
    if not article_links:
        print("Nu s-au găsit linkuri pe această pagină. Oprire...")
        break
    all_article_links.extend(article_links)
    page_number += 1

print(f"Număr total de articole găsite: {len(all_article_links)}")

# Funcție pentru a descărca un singur articol
def download_article(article_link):
    try:
        print(f"Descărcarea articolului de la {article_link}...")
        article_response = session.get(article_link)
        article_response.raise_for_status()  # Verifică dacă există erori HTTP
        article_html = article_response.text
        article_soup = BeautifulSoup(article_html, "html.parser")

        # Extrage titlul articolului și înlătură caracterele speciale
        title_element = article_soup.find("h1")
        if title_element:
            title = title_element.text.strip()
            title = title.replace("/", "-")  # înlocuiește "/" cu "-" pentru a evita erorile legate de fișiere
            title = title.replace(":", "-")  # înlocuiește ":" cu "-" pentru a evita erorile legate de fișiere

            # Extrage categoria articolului (dacă există)
            category_element = article_soup.find("span", class_="category")
            if category_element:
                category = category_element.text.strip()
            else:
                category = "Uncategorized"

            # Crează subdirectorul pentru categoria articolului (dacă există)
            category_directory = os.path.join(output_directory, category)
            os.makedirs(category_directory, exist_ok=True)

            # Converteste HTML-ul in Markdown
            markdown_content = convert_html_to_markdown(article_soup)

            # Salvează conținutul articolului în format Markdown
            file_path = os.path.join(category_directory, f"{title}.md")
            with open(file_path, "w", encoding="utf-8") as markdown_file:
                markdown_file.write(f"# {title}\n\n")
                markdown_file.write(markdown_content)
            print(f"Articol descărcat și salvat la {file_path}")
        else:
            print(f"Titlul nu a fost găsit pentru {article_link}. Articolul nu a fost salvat.")
    except requests.exceptions.RequestException as e:
        print(f"Eroare la descărcarea articolului de la {article_link}: {e}")

# Functie pentru a converti HTML-ul in Markdown
def convert_html_to_markdown(soup):
    markdown_content = ""
    for element in soup.descendants:
        if element.name == "h1":
            markdown_content += f"# {element.text.strip()}\n\n"
        elif element.name == "h2":
            markdown_content += f"## {element.text.strip()}\n\n"
        elif element.name == "h3":
            markdown_content += f"### {element.text.strip()}\n\n"
        elif element.name == "h4":
            markdown_content += f"#### {element.text.strip()}\n\n"
        elif element.name == "h5":
            markdown_content += f"##### {element.text.strip()}\n\n"
        elif element.name == "h6":
            markdown_content += f"###### {element.text.strip()}\n\n"
        elif element.name == "p":
            markdown_content += f"{element.text.strip()}\n\n"
    return markdown_content

# Descarcă fiecare articol utilizând ThreadPoolExecutor
print("Descărcarea articolelor...")
with ThreadPoolExecutor() as executor:
    executor.map(download_article, all_article_links)

print("Descărcare completă!")

