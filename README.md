# WP Download

### Descriere
---
Acest script Python este destinat extragerii și descărcării articolelor de pe un site WordPress specificat și conversia acestora în format Markdown. Este util pentru persoanele care doresc să păstreze o copie locală a articolelor de pe un site WordPress sau să le integreze în alte proiecte.

### Funcționalități
---
- Extrage automat linkurile către articolele de pe un site WordPress specificat.
- Descarcă fiecare articol identificat, convertindu-l în format Markdown.
- Salvează fiecare articol descărcat într-un director local, structurat în funcție de categoria articolului.

### Utilizare
---
1. Modificați variabilele `wordpress_url` și `output_directory` pentru a reflecta URL-ul site-ului WordPress dorit și directorul în care doriți să salvați articolele descărcate.
2. Rulați scriptul. Acesta va extrage automat linkurile către articole, le va descărca și le va salva în directorul specificat.

### Dependențe
---
Pentru a rula acest script, aveți nevoie de următoarele dependențe Python:

- `requests`: pentru a efectua cereri HTTP către site-ul WordPress.
- `BeautifulSoup`: pentru a analiza și extrage informații din paginile HTML ale articolelor.
- `concurrent.futures`: pentru a gestiona descărcarea simultană a mai multor articole.

### Sisteme de operare
---
Acest script poate fi rulat pe orice sistem de operare care suportă Python și dependențele menționate mai sus. A fost testat pe Fedora Linux.
